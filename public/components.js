'use strict';

angular.module('reversi', ['reversi.factory']);
'use strict';

angular.module('reversi').component('reversi', {
  templateUrl: './reversi/reversi.template.html',
  controller: ['Game', function (Game) {
    var ctx = this;
    ctx.tablero = {};
    ctx.winnerGamme = false;
    ctx.whiteCount = 0;
    ctx.blackCount = 0;
    ctx.Name = 'Reversi';
    ctx.playerWon = '';

    ctx.Init = function () {
      Game.getGame({}, { token: '9b4702ae-0861-496b-8c2e-95a9744cf583' }).$promise.then(function (res) {
        ctx.boardRows = res.boardRows;
        setCurrentPlayer(res);
        checkWinner(res);
      });
    };

    ctx.ResetGame = function () {
      Game.delete({}, { token: '9b4702ae-0861-496b-8c2e-95a9744cf583' }).$promise.then(function (res) {
        console.log('Juego reiniciado');
        ctx.Init();
      });
    };

    ctx.Move = function (x, y) {
      Game.movements({ x: x, y: y }, { token: '9b4702ae-0861-496b-8c2e-95a9744cf583' }).$promise.then(function (res) {
        ctx.Init();
      }, function (err) {
        console.log('error');
        if (err.status === 400) {
          alert('Movimiento Invalido');
        } else if (err.status === 409) {
          if (ctx.winnerGamme) {
            alert('El juego ha terminado');
          } else {
            alert('Casilla ocupada');
          }
        }
      });
    };

    var setCurrentPlayer = function setCurrentPlayer(res) {
      ctx.currentPlayer = res.currentPlayer === 'BLACK' ? 'NEGRO' : 'BLANCO';
    };

    var checkWinner = function checkWinner(res) {
      ctx.whiteCount = res.whiteCount;
      ctx.blackCount = res.blackCount;
      ctx.winnerGamme = res.gameState === 'FINALIZED' ? true : false;

      if (ctx.winnerGamme) {
        if (ctx.whiteCount === ctx.blackCount) {
          ctx.playerWon = 'EMPATE';
        } else if (ctx.blackCount > ctx.whiteCount) {
          ctx.playerWon = 'NEGRO';
        } else if (ctx.whiteCount > ctx.blackCount) {
          ctx.playerWon = 'BLANCO';
        }
      }
    };
  }]
});
'use strict';

angular.module('reversi.factory', ['ngResource']);
'use strict';

angular.module('reversi.factory').factory('Game', ['$resource', function ($resource) {
  return $resource('http://34.213.192.159:9000/reversi/game/:movements/', {}, {
    movements: { method: 'POST', params: { movements: 'movements', token: '9b4702ae-0861-496b-8c2e-95a9744cf583' } },
    getGame: { method: 'GET', params: { token: '9b4702ae-0861-496b-8c2e-95a9744cf583' } },
    delete: { method: 'DELETE', params: { token: '9b4702ae-0861-496b-8c2e-95a9744cf583' } }
  });
}]);
'use strict';

angular.module('reversiApp', ['ngRoute', 'reversi', 'reversi.factory']).config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  $routeProvider.when('/', {
    template: '<reversi></reversi>'
  }).otherwise({
    redirectTo: '/'
  });

  $locationProvider.html5Mode(true);
}]).factory('ServiceFactory', ['$http', function ($http) {
  return function (entity, serviceUrl) {

    return new Promise(function (resolve, reject) {
      $http.post(serviceUrl, entity).then(resolve, reject);
    });
  };
}]);