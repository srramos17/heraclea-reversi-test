'use strict'

angular.module('reversi')
.component('reversi', {
  templateUrl: './reversi/reversi.template.html',
  controller: ['Game', function(Game) {
    let ctx = this
    ctx.tablero = {}
    ctx.winnerGamme = false
    ctx.whiteCount = 0
    ctx.blackCount = 0
    ctx.Name = 'Reversi'
    ctx.playerWon = ''

    ctx.Init = () => {
        Game.getGame({}, {token: '9b4702ae-0861-496b-8c2e-95a9744cf583'}).$promise.then((res) => {
          ctx.boardRows = res.boardRows
          setCurrentPlayer(res)
          checkWinner(res)
        })
    }

    ctx.ResetGame = () => {
      Game.delete({}, {token: '9b4702ae-0861-496b-8c2e-95a9744cf583'}).$promise.then((res) => {
          console.log('Juego reiniciado')
          ctx.Init()
        })
    }

    ctx.Move = (x, y) => {
      Game.movements({x:x, y:y}, {token: '9b4702ae-0861-496b-8c2e-95a9744cf583'}).$promise.then((res) => {
        ctx.Init()
      }, (err) => {
        console.log('error')
        if (err.status === 400) {
          alert('Movimiento Invalido')
        }
        else if (err.status === 409) {
          if (ctx.winnerGamme) {
            alert('El juego ha terminado')
          } else {
            alert('Casilla ocupada')
          }
        }
      })
    }

    const setCurrentPlayer = (res) => {
      ctx.currentPlayer = res.currentPlayer === 'BLACK' ? 'NEGRO' : 'BLANCO'
    }

    const checkWinner = (res) => {
      ctx.whiteCount = res.whiteCount
      ctx.blackCount = res.blackCount
      ctx.winnerGamme = res.gameState === 'FINALIZED' ? true : false

      if (ctx.winnerGamme) {
        if (ctx.whiteCount === ctx.blackCount) {
          ctx.playerWon = 'EMPATE'
        }
        else if (ctx.blackCount > ctx.whiteCount) {
          ctx.playerWon = 'NEGRO'
        }
        else if (ctx.whiteCount > ctx.blackCount) {
          ctx.playerWon = 'BLANCO'
        }
      }
    }
  }]
})